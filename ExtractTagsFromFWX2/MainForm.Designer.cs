﻿/*
 * Created by SharpDevelop.
 * User: timofeev
 * Date: 06.06.2014
 * Time: 8:50
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ExtractTagsFromFWX2
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.menuStrip = new System.Windows.Forms.MenuStrip();
			this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.lbTags = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lbInfo = new System.Windows.Forms.ListBox();
			this.propertyGrid = new System.Windows.Forms.PropertyGrid();
			this.label2 = new System.Windows.Forms.Label();
			this.textNameFilter = new System.Windows.Forms.TextBox();
			this.btFilter = new System.Windows.Forms.Button();
			this.cbTagTypes = new System.Windows.Forms.ComboBox();
			this.rbUseNameFilter = new System.Windows.Forms.RadioButton();
			this.rbUseTypeFilter = new System.Windows.Forms.RadioButton();
			this.menuStrip.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip
			// 
			this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.файлToolStripMenuItem});
			this.menuStrip.Location = new System.Drawing.Point(0, 0);
			this.menuStrip.Name = "menuStrip";
			this.menuStrip.Size = new System.Drawing.Size(740, 24);
			this.menuStrip.TabIndex = 0;
			this.menuStrip.Text = "menuStrip1";
			// 
			// файлToolStripMenuItem
			// 
			this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.openToolStripMenuItem,
									this.exitMenuItem});
			this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
			this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
			this.файлToolStripMenuItem.Text = "Файл";
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
			this.openToolStripMenuItem.Text = "Открыть";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItemClick);
			// 
			// exitMenuItem
			// 
			this.exitMenuItem.Name = "exitMenuItem";
			this.exitMenuItem.Size = new System.Drawing.Size(121, 22);
			this.exitMenuItem.Text = "Выход";
			this.exitMenuItem.Click += new System.EventHandler(this.ExitMenuItemClick);
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.toolStripStatusLabel1});
			this.statusStrip.Location = new System.Drawing.Point(0, 588);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(740, 22);
			this.statusStrip.TabIndex = 1;
			this.statusStrip.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(115, 17);
			this.toolStripStatusLabel1.Text = "fileNameStatusLabel";
			// 
			// openFileDialog
			// 
			this.openFileDialog.FileName = "PDATA.FWX";
			this.openFileDialog.Filter = "FWX|*.fwx|All|*.*";
			// 
			// lbTags
			// 
			this.lbTags.FormattingEnabled = true;
			this.lbTags.Location = new System.Drawing.Point(12, 85);
			this.lbTags.Name = "lbTags";
			this.lbTags.ScrollAlwaysVisible = true;
			this.lbTags.Size = new System.Drawing.Size(476, 199);
			this.lbTags.TabIndex = 2;
			this.lbTags.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LbTagsMouseDoubleClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 65);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 17);
			this.label1.TabIndex = 3;
			this.label1.Text = "Теги";
			// 
			// lbInfo
			// 
			this.lbInfo.FormattingEnabled = true;
			this.lbInfo.Location = new System.Drawing.Point(494, 85);
			this.lbInfo.Name = "lbInfo";
			this.lbInfo.Size = new System.Drawing.Size(231, 199);
			this.lbInfo.TabIndex = 4;
			// 
			// propertyGrid
			// 
			this.propertyGrid.Location = new System.Drawing.Point(12, 290);
			this.propertyGrid.Name = "propertyGrid";
			this.propertyGrid.Size = new System.Drawing.Size(713, 286);
			this.propertyGrid.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(320, 38);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 17);
			this.label2.TabIndex = 6;
			this.label2.Text = "Фильтр:";
			// 
			// textNameFilter
			// 
			this.textNameFilter.Location = new System.Drawing.Point(378, 35);
			this.textNameFilter.Name = "textNameFilter";
			this.textNameFilter.Size = new System.Drawing.Size(157, 20);
			this.textNameFilter.TabIndex = 7;
			// 
			// btFilter
			// 
			this.btFilter.Location = new System.Drawing.Point(668, 33);
			this.btFilter.Name = "btFilter";
			this.btFilter.Size = new System.Drawing.Size(57, 23);
			this.btFilter.TabIndex = 8;
			this.btFilter.Text = "Фильтр";
			this.btFilter.UseVisualStyleBackColor = true;
			this.btFilter.Click += new System.EventHandler(this.BtFilterByNameClick);
			// 
			// cbTagTypes
			// 
			this.cbTagTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbTagTypes.FormattingEnabled = true;
			this.cbTagTypes.Location = new System.Drawing.Point(541, 35);
			this.cbTagTypes.Name = "cbTagTypes";
			this.cbTagTypes.Size = new System.Drawing.Size(121, 21);
			this.cbTagTypes.TabIndex = 9;
			// 
			// rbUseNameFilter
			// 
			this.rbUseNameFilter.Location = new System.Drawing.Point(378, 60);
			this.rbUseNameFilter.Name = "rbUseNameFilter";
			this.rbUseNameFilter.Size = new System.Drawing.Size(104, 20);
			this.rbUseNameFilter.TabIndex = 10;
			this.rbUseNameFilter.TabStop = true;
			this.rbUseNameFilter.Text = "Использовать";
			this.rbUseNameFilter.UseVisualStyleBackColor = true;
			// 
			// rbUseTypeFilter
			// 
			this.rbUseTypeFilter.Location = new System.Drawing.Point(541, 60);
			this.rbUseTypeFilter.Name = "rbUseTypeFilter";
			this.rbUseTypeFilter.Size = new System.Drawing.Size(104, 20);
			this.rbUseTypeFilter.TabIndex = 11;
			this.rbUseTypeFilter.TabStop = true;
			this.rbUseTypeFilter.Text = "Использовать";
			this.rbUseTypeFilter.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(740, 610);
			this.Controls.Add(this.rbUseTypeFilter);
			this.Controls.Add(this.rbUseNameFilter);
			this.Controls.Add(this.cbTagTypes);
			this.Controls.Add(this.btFilter);
			this.Controls.Add(this.textNameFilter);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.propertyGrid);
			this.Controls.Add(this.lbInfo);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lbTags);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.menuStrip);
			this.MainMenuStrip = this.menuStrip;
			this.Name = "MainForm";
			this.Text = "ExtractTagsFromFWX 2.0 by Tmofeev Ilya, 2014";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.menuStrip.ResumeLayout(false);
			this.menuStrip.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.RadioButton rbUseTypeFilter;
		private System.Windows.Forms.RadioButton rbUseNameFilter;
		private System.Windows.Forms.ComboBox cbTagTypes;
		private System.Windows.Forms.Button btFilter;
		private System.Windows.Forms.TextBox textNameFilter;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.PropertyGrid propertyGrid;
		private System.Windows.Forms.ListBox lbInfo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox lbTags;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
		private System.Windows.Forms.MenuStrip menuStrip;
	}
}
