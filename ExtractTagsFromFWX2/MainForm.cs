﻿/*
 * Created by SharpDevelop.
 * User: timofeev
 * Date: 06.06.2014
 * Time: 8:50
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace ExtractTagsFromFWX2
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		List<WinCCTag> tagList = null;
		List<WinCCTagAdress> tagAdressList = null;
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		void ExitMenuItemClick(object sender, EventArgs e)
		{
			Application.Exit();
		}
		
		bool CompareUShortArrays(ushort[] mas1, ushort[] mas2)
		{
			if (mas1.Length != mas2.Length)
				return false;
			
			for (int i = 0; i < mas1.Length; i++)
			{
				if (mas1[i] != mas2[i])
					return false;
			}
			return true;	
		}
		
		/// <summary>
		/// Чтение двух байт в формате BigEndian
		/// </summary>
		/// <param name="br"></param>
		/// <returns></returns>
		ushort ReadBigEndianUShort(BinaryReader br)
		{
			byte[] byte_mas = br.ReadBytes(2);
			ushort res;
			if (byte_mas.Length == 2)
				res = (ushort)(byte_mas[1] | byte_mas [0] << 8);
			else
				res = 0;
			return res;
		}
		
		void ReadAdress(string FileName)
		{
			BinaryReader br = new BinaryReader(File.Open(FileName, FileMode.Open, FileAccess.Read, FileShare.None));
			lbInfo.Items.Clear();
			
			//DARALINKREAD_WR ключевое слово
			ushort[] DATALINK_READWR = 
					{0x4400, 0x4100, 0x5400, 0x4100, 
					0x4C00, 0x4900, 0x4E00, 0x4B00, 
					0x5F00, 0x5200, 0x4500, 0x4100,
					0x4400, 0x5700, 0x5200};
			long DATALINK_READWR_position = 0;
			
			//CONNECTION_S7 ключевое слово
			ushort[] CONNECTION_S7 = 
					{0x4300, 0x4F00, 0x4E00, 0x4E00, 
					0x4500, 0x4300, 0x5400, 0x4900, 
					0x4F00, 0x4E00, 0x5F00, 0x5300, 0x3700};
			long CONNECTION_S7_position = 0;
			
			ushort[] ADRESS = {0x0201, 0x0000, 0x0900, 0x0000};
			
			ushort[] searchBuf = new ushort[DATALINK_READWR.Length];
			
			while(br.BaseStream.Position < br.BaseStream.Length)
			{
				for (int i = 0; i < DATALINK_READWR.Length; i++)
				{
					searchBuf[i] = ReadBigEndianUShort(br);
				}
				if (CompareUShortArrays(searchBuf, DATALINK_READWR))
				{
					lbInfo.Items.Add("Нашли DATALINK_READWR на " + br.BaseStream.Position);
					DATALINK_READWR_position = br.BaseStream.Position;
					break;
				}
				br.BaseStream.Position = br.BaseStream.Position - sizeof(ushort) * (DATALINK_READWR.Length - 1);
			}
			
			
			searchBuf = new ushort[CONNECTION_S7.Length];
			
			while(br.BaseStream.Position < br.BaseStream.Length)
			{
				for (int i = 0; i < CONNECTION_S7.Length; i++)
				{
					searchBuf[i] = ReadBigEndianUShort(br);
				}
				if (CompareUShortArrays(searchBuf, CONNECTION_S7))
				{
					lbInfo.Items.Add("Нашли CONNECTION_S7 на " + br.BaseStream.Position);
					CONNECTION_S7_position = br.BaseStream.Position;
					break;
				}
				br.BaseStream.Position = br.BaseStream.Position - sizeof(ushort) * (CONNECTION_S7.Length - 1);
			}
			
			br.BaseStream.Position = DATALINK_READWR_position;
			
			tagAdressList = new List<WinCCTagAdress>();
			
			int tagCounter = 0;
			searchBuf = new ushort[ADRESS.Length];
			
			while(br.BaseStream.Position <= CONNECTION_S7_position)
			{
				for (int i = 0; i < ADRESS.Length; i++)
				{
					searchBuf[i] = ReadBigEndianUShort(br);
				}
				
				if (CompareUShortArrays(searchBuf, ADRESS))
				{
					WinCCTagAdress winCCTagAdress = new WinCCTagAdress();
					winCCTagAdress.Num = tagCounter++;
					winCCTagAdress.AdressType = (WinCCTagAdressType)ReadBigEndianUShort(br);
					winCCTagAdress.TagType = (WinCCTagType)br.ReadByte();
					winCCTagAdress.DBNumber = br.ReadUInt16();
					winCCTagAdress.WordNumber = br.ReadUInt16();
					winCCTagAdress.BitNumber = br.ReadByte();
					
					tagAdressList.Add(winCCTagAdress);
				}
				
				br.BaseStream.Position = br.BaseStream.Position - sizeof(ushort) * (ADRESS.Length - 1);
			}
			
			lbInfo.Items.Add("Нашли адресов — " + tagCounter);
			
			br.Close();
		}
		
		void ReadTags(string FileName)
		{
			BinaryReader br = new BinaryReader(File.Open(FileName, FileMode.Open, FileAccess.Read, FileShare.None));
			
			//DARALINKREAD_WR ключевое слово
			ushort[] VAR = 
					{0x5600, 0x4100, 0x5200};
			long VAR_position = 0;
			
			//VIEW_IN ключевое слово
			ushort[] VIEW_IN = 
					{0x5600, 0x4900, 0x4500, 0x5700, 
					0x5F00, 0x4900, 0x4E00};
			long VIEW_IN_position = 0;
			
			ushort[] TAG = {0x0000, 0x0300};
			
			ushort[] searchBuf = new ushort[VAR.Length];
			
			while(br.BaseStream.Position < br.BaseStream.Length)
			{
				for (int i = 0; i < VAR.Length; i++)
				{
					searchBuf[i] = ReadBigEndianUShort(br);
				}
				if (CompareUShortArrays(searchBuf, VAR))
				{
					lbInfo.Items.Add("Нашли VAR на " + br.BaseStream.Position);
					VAR_position = br.BaseStream.Position;
					break;
				}
				br.BaseStream.Position = br.BaseStream.Position - sizeof(ushort) * (VAR.Length - 1);
			}
			
			
			searchBuf = new ushort[VIEW_IN.Length];
			
			while(br.BaseStream.Position < br.BaseStream.Length)
			{
				for (int i = 0; i < VIEW_IN.Length; i++)
				{
					searchBuf[i] = ReadBigEndianUShort(br);
				}
				if (CompareUShortArrays(searchBuf, VIEW_IN))
				{
					lbInfo.Items.Add("Нашли VIEW_IN на " + br.BaseStream.Position);
					VIEW_IN_position = br.BaseStream.Position;
					break;
				}
				br.BaseStream.Position = br.BaseStream.Position - sizeof(ushort) * (VIEW_IN.Length - 1);
			}
			
			br.BaseStream.Position = VAR_position;
			
			tagList = new List<WinCCTag>();
			
			int tagCounter = 0;
			searchBuf = new ushort[TAG.Length];
			
			while(br.BaseStream.Position <= VIEW_IN_position)
			{
				for (int i = 0; i < TAG.Length; i++)
				{
					searchBuf[i] = ReadBigEndianUShort(br);
				}
				
				if (CompareUShortArrays(searchBuf, TAG))
				{
					WinCCTag winCCTag = new WinCCTag();
					
					ushort tmpUShort;
					tmpUShort = ReadBigEndianUShort(br);
					if (tmpUShort == 0x0000) //надо пропустить два байта
						continue;
					
					tagCounter++;	
						
					byte tmpByte = br.ReadByte();
					while(tmpByte != 0x02)
					{
						if (tmpByte != 0x00)
							winCCTag.Name += (char)(tmpByte);
						tmpByte = br.ReadByte();
					}
					tmpByte = br.ReadByte(); //для четности
					
					//пропускаем ненужности
					tmpUShort = ReadBigEndianUShort(br);
					while(tmpUShort != 0x3500)
						tmpUShort = ReadBigEndianUShort(br);
					
					tmpUShort = br.ReadUInt16();
					foreach(WinCCTagAdress item in tagAdressList)
					{
						if (item.Num == tmpUShort)
						{
							winCCTag.Adress = item.S7Adress;
							winCCTag.Type = item.TagType;
							break;
						}
					}
					
					tagList.Add(winCCTag);
				}
				
				br.BaseStream.Position = br.BaseStream.Position - sizeof(ushort) * (TAG.Length - 1);
			}
			
			lbInfo.Items.Add("Нашли тегов — " + tagCounter);
			
			br.Close();
		}
		
		/// <summary>
		/// Вывод полученных тегов в ListBox на форме
		/// </summary>
		void PrintTags()
		{
			lbTags.Items.Clear();
			foreach(WinCCTag item in tagList)
			{
				lbTags.Items.Add(item);
			}
		}
		
		void PrintTagsByName(string tagName)
		{
			lbTags.Items.Clear();
			foreach(WinCCTag item in tagList)
			{
				if (item.Name.Contains(tagName))
					lbTags.Items.Add(item);
			}
		}
		
		void PrintTagsByType(WinCCTagType tagType)
		{
			lbTags.Items.Clear();
			foreach(WinCCTag item in tagList)
			{
				if (item.Type == tagType)
					lbTags.Items.Add(item);
			}
		}
		
		void PrintAdress()
		{
			lbTags.Items.Clear();
			foreach(WinCCTagAdress item in tagAdressList)
			{
				lbTags.Items.Add(item);
			}
		}
		
		void OpenToolStripMenuItemClick(object sender, EventArgs e)
		{
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				statusStrip.Items[0].Text = openFileDialog.FileName;
				ReadAdress(openFileDialog.FileName);
				ReadTags(openFileDialog.FileName);
				PrintTags();
			}
		}
		
		void LbTagsMouseDoubleClick(object sender, MouseEventArgs e)
		{
			propertyGrid.SelectedObject = (sender as ListBox).SelectedItem;
		}
		
		void BtFilterByNameClick(object sender, EventArgs e)
		{
			WinCCTagType filterType = (WinCCTagType)Enum.Parse(typeof(WinCCTagType), cbTagTypes.SelectedValue.ToString());
			string filterName = textNameFilter.Text;
			
			if (rbUseNameFilter.Checked && (filterName.Length > 0))
				PrintTagsByName(filterName);
			
			if (rbUseTypeFilter.Checked)
				PrintTagsByType(filterType);
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			cbTagTypes.DataSource = Enum.GetValues(typeof(WinCCTagType));
		}
		
		
	}
}
