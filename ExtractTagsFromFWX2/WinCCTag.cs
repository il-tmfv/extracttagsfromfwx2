﻿/*
 * Created by SharpDevelop.
 * User: timofeev
 * Date: 06.06.2014
 * Time: 9:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;

namespace ExtractTagsFromFWX2 
{
	public enum WinCCTagType : byte
	{
		Char = 0x00,
		Int = 0x02,
		Word = 0x03,
		Byte = 0x01,
		TimeOfDay = 0x0C,
		DWord = 0x05,
		Real = 0x06,
		Bool = 0x07,
		Timer = 0x09,
		Counter = 0x0A,
		Date = 0x0B,
		Time = 0x04,
		DateAndTime = 0x0D,
		DInt = 0x08
	}
	
	/// <summary>
	/// Description of WinCCTag.
	/// </summary>
	public class WinCCTag
	{
		public WinCCTag()
		{
			
		} 
		
		public WinCCTag(string Name, string Adress, WinCCTagType Type)
		{
			this.Name = Name;
			this.Adress = Adress;
			this.Type = Type;
		}
		
		private string adress;
		private string name;
		private WinCCTagType type;
		
		[ReadOnly(true)]                            
    	[Description("Адрес тега в формате S7S5")]   
    	[Category("WinCC Flexible Tag")]         
    	[DisplayName("S7S5Adress")]       
		public string Adress {
			get { return adress; }
			set { adress = value; }
		}

    	[ReadOnly(true)]                            
    	[Description("Имя тега")]   
    	[Category("WinCC Flexible Tag")]         
    	[DisplayName("Name")]
		public string Name {
			get { return name; }
			set { name = value; }
		}

    	[ReadOnly(true)]                            
    	[Description("Тип тега")]   
    	[Category("WinCC Flexible Tag")]         
    	[DisplayName("Type")]
		public WinCCTagType Type {
			get { return type; }
			set { type = value; }
		}
		
		public override string ToString()
		{
			return string.Format("{0} [Type: {1}, Adress: {2}]", this.Name, this.Type, this.Adress);
		}
	}
}
