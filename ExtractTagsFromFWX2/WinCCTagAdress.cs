﻿/*
 * Created by SharpDevelop.
 * User: timofeev
 * Date: 06.06.2014
 * Time: 10:09
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace ExtractTagsFromFWX2
{
	public enum WinCCTagAdressType : ushort
	{
		Q = 0x0500,
		M = 0x0200,
		DB = 0x0000,
		I = 0x0400,
		C = 0x0600
	}
	
	/// <summary>
	/// Description of WinCCTagAdress.
	/// </summary>
	public class WinCCTagAdress
	{
		public WinCCTagAdress()
		{
		}
		
		private int num;
		private WinCCTagAdressType adressType;
		private WinCCTagType tagType;
		private int bitNumber;
		private int wordNumber;
		private int dBNumber;
		
		public string AdressLetter
		{
			get 
			{
				switch(this.TagType)
				{
					case WinCCTagType.Bool:	return "X";	break;
					case WinCCTagType.Int:	return "W";	break;
					case WinCCTagType.Word:	return "W";	break;
					case WinCCTagType.Byte:	return "B";	break;
					case WinCCTagType.TimeOfDay:	return "D";	break;
					case WinCCTagType.DWord:	return "D";	break;
					case WinCCTagType.Real:	return "D";	break;
					case WinCCTagType.Timer:	return "W";	break;
					case WinCCTagType.Counter:	return "W";	break;
					case WinCCTagType.Date:	return "W";	break;
					case WinCCTagType.Time:	return "D";	break;
					case WinCCTagType.DateAndTime:	return "B";	break;
					case WinCCTagType.DInt:	return "D";	break;
					default: return "*"; break;
				}
			}
		}
			
		
		public string S7Adress
		{
			get
			{
				string res = "";
				switch(this.AdressType)
				{
					case WinCCTagAdressType.C:
						res = String.Format("C{0}", this.WordNumber);
						break;
						
					case WinCCTagAdressType.DB:
						if (TagType == WinCCTagType.Bool)
							res = String.Format("DB{0}.DBX{1}.{2}", this.DBNumber, this.WordNumber, this.BitNumber);
						else
							res = String.Format("DB{0}.DB{1}{2}", this.DBNumber, this.AdressLetter, this.WordNumber);
						break;
						
					case WinCCTagAdressType.I:
						if (TagType == WinCCTagType.Bool)
							res = String.Format("I{0}.{1}", this.WordNumber, this.BitNumber);
						else
							res = String.Format("I{0}{1}", this.AdressLetter, this.WordNumber);
						break;
						
					case WinCCTagAdressType.Q:
						if (TagType == WinCCTagType.Bool)
							res = String.Format("Q{0}.{1}", this.WordNumber, this.BitNumber);
						else
							res = String.Format("Q{0}{1}", this.AdressLetter, this.WordNumber);
						break;
						
					case WinCCTagAdressType.M:
						if (TagType == WinCCTagType.Bool)
							res = String.Format("M{0}.{1}", this.WordNumber, this.BitNumber);
						else
							res = String.Format("M{0}{1}", this.AdressLetter, this.WordNumber);
						break;
				}
				
				return res;
			}
		}
		
		public int Num {
			get { return num; }
			set { num = value; }
		}

		public WinCCTagAdressType AdressType {
			get { return adressType; }
			set { adressType = value; }
		}

		public WinCCTagType TagType {
			get { return tagType; }
			set { tagType = value; }
		}

		public int BitNumber {
			get { return bitNumber; }
			set { bitNumber = value; }
		}

		public int WordNumber {
			get { return wordNumber; }
			set { wordNumber = value; }
		}

		public int DBNumber {
			get { return dBNumber; }
			set { dBNumber = value; }
		}
		
		public override string ToString()
		{
			return String.Format("[{0}]", this.S7Adress);
		}		
	}
}
